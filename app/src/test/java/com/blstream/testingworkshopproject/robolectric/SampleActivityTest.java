package com.blstream.testingworkshopproject.robolectric;

import android.os.Build;

import com.blstream.testingworkshopproject.BuildConfig;
import com.blstream.testingworkshopproject.example.robolectric.SampleActivity;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by sswierczek on 9/17/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.LOLLIPOP, constants = BuildConfig.class)
public class SampleActivityTest {

    @Test
    public void justSimpleActivityCreation() {
        SampleActivity activity = Robolectric.setupActivity(SampleActivity.class);

        String result = activity.textView.getText().toString();

        Assert.assertEquals("Test test", result);
    }

}