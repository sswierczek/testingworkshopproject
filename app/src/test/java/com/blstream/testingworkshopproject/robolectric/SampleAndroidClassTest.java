package com.blstream.testingworkshopproject.robolectric;

import android.content.Context;
import android.os.Build;

import com.blstream.testingworkshopproject.BuildConfig;
import com.blstream.testingworkshopproject.R;
import com.blstream.testingworkshopproject.example.robolectric.SampleAndroidClass;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by sswierczek on 9/13/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.LOLLIPOP, constants = BuildConfig.class)
public class SampleAndroidClassTest {

    private Context context;

    @Before
    public void beforeEachTest() {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void GIVEN_validParametrizedResource_WHEN_passOneArgument_THEN_returnStringWithGivenArgument() {
        int validParametrizedResourceResId = R.string.valid_parametrized_string;
        String argument = "John";
        String expectedString = "Hello John";

        SampleAndroidClass sampleAndroidClass = new SampleAndroidClass(context);
        String result = sampleAndroidClass.getStringResourceWithArguments(validParametrizedResourceResId, argument);

        Assert.assertEquals(expectedString, result);
    }
}
