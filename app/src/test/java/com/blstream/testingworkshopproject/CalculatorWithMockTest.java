package com.blstream.testingworkshopproject;

import com.blstream.testingworkshopproject.example.Calculator;
import com.blstream.testingworkshopproject.example.Memory;

import org.easymock.EasyMock;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by sswierczek on 9/12/16.
 */
public class CalculatorWithMockTest {

    @Test
    public void GIVEN_valueInMemory_WHEN_addedCorrectValues_THEN_resultPlusMemoryValue() {
        Memory memoryMock = EasyMock.createMock(Memory.class);
        EasyMock.expect(memoryMock.getMemoryValue()).andReturn(5);
        Calculator example = new Calculator(memoryMock);
        EasyMock.replay(memoryMock);

        int result = example.add(2, 2);

        assertEquals(9, result);
        EasyMock.verify(memoryMock);
    }
}
