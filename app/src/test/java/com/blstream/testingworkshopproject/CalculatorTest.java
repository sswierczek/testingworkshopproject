package com.blstream.testingworkshopproject;

import com.blstream.testingworkshopproject.example.Calculator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void GIVEN_valueInMemory_WHEN_addedCorrectValues_THEN_resultPlusMemoryValue() {
        Calculator example = new Calculator();
        example.addToMemory(5);

        int result = example.add(2, 2);

        assertEquals(9, result);
    }

    @Test
    public void WHEN_addedCorrectValues_THEN_returnAdditionResult() {
        Calculator example = new Calculator();

        int result = example.add(2, 2);

        assertEquals(4, result);
    }
}