package com.blstream.testingworkshopproject.example;

/**
 * Created by sswierczek on 9/12/16.
 */
public class Memory {

    private int memory = 0;

    public void addToMemory(int value) {
        memory += value;
    }

    public int getMemoryValue() {
        return memory;
    }
}
