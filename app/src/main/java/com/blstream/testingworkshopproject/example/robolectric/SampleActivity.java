package com.blstream.testingworkshopproject.example.robolectric;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.blstream.testingworkshopproject.R;

public class SampleActivity extends AppCompatActivity {

    public TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        textView = (TextView) findViewById(R.id.textView);
    }
}
