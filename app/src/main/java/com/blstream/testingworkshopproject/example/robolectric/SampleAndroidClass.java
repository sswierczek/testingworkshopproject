package com.blstream.testingworkshopproject.example.robolectric;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by sswierczek on 9/13/16.
 */
public class SampleAndroidClass {

    private final Context context;

    public SampleAndroidClass(Context context) {
        this.context = context;
    }

    public String getStringResourceWithArguments(@StringRes int stringResId, String... args) {
        return String.format(context.getResources().getString(stringResId), args);
    }
}
