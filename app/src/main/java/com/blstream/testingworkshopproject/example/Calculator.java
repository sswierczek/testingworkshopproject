package com.blstream.testingworkshopproject.example;

/**
 * Created by sswierczek on 9/12/16.
 */
public class Calculator {

    private final Memory memory;

    public Calculator() {
        this(new Memory());
    }

    public Calculator(Memory memory) {
        this.memory = memory;
    }

    public void addToMemory(int value) {
        memory.addToMemory(value);
    }

    public int add(int a, int b) {
        return memory.getMemoryValue() + a + b;
    }
}